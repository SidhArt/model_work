class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :address
      t.string :email
      t.integer :role

      t.timestamps
    end
    add_index :users, 'lower(email)', unique: true
    add_index :users, 'lower(username)', unique: true
  end
end
