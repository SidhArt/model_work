json.extract! user, :id, :username, :address, :email, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
